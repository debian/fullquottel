fullquottel (0.1.5-2) unstable; urgency=medium

  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.7.0.
  * Update lintian override info format.
  * debian/rules: add empty override_dh_auto_test.

 -- gregor herrmann <gregoa@debian.org>  Thu, 18 Apr 2024 23:55:37 +0200

fullquottel (0.1.5-1) unstable; urgency=medium

  * Import upstream version 0.1.5.
    - Fix for C++17 / gcc-11
      Closes: #984141
  * Update years of upstream and packaging copyright.
  * Update expected version in debian/tests/help-version.
  * Declare compliance with Debian Policy 4.5.1.
  * Update 'DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow' to '=+all'.
  * Bump debhelper-compat to 13.
  * Add lintian overrides for hints about Doxygen files.

 -- gregor herrmann <gregoa@debian.org>  Sun, 15 Aug 2021 14:32:57 +0200

fullquottel (0.1.4-1) unstable; urgency=medium

  * New upstream release. (No code changes.)
  * Verify OpenPGP signature of upstream tarball.
  * Use uscan version 4 in debian/watch.
  * Update years of packaging copyright.
  * Update Vcs-* fields.
  * Use HTTPS for several URLs.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Bump debhelper-compat to 12.
  * Drop unnecessary dependency on dh-autoreconf.
  * Set bindnow linker flag in debian/rules.
  * Add superficial autopkgtest.

 -- gregor herrmann <gregoa@debian.org>  Sat, 02 May 2020 17:24:45 +0200

fullquottel (0.1.3-1) unstable; urgency=low

  * Update Vcs-* headers.

  * New upstream release.
  * Drop manpage.patch, applied upstream.
  * debian/copyright: update formatting and copyright years.
  * Bump debhelper compatibility level to 9.
  * Set Standards-Version to 3.9.4 (no changes).
  * Switch from autotools-dev to dh-autoconf.

 -- gregor herrmann <gregoa@debian.org>  Sun, 05 May 2013 19:07:49 +0200

fullquottel (0.1.2-2) unstable; urgency=low

  * Move upstream URL from the description to the new Homepage field
    (closes: #615342).
  * Change XS-Vcs-* fields to Vcs-*.
  * Change debian/copyright to the new machine-readable format.
  * Set Standards-Version to 3.9.1 (no changes).
  * Switch to source format 3.0 (quilt).
  * Switch to debhelper 8; use the autotools-dev debhelper sequence in
    debian/rules.
  * Update my email address.
  * Split out the changes to the manpage into a proper patch.
  * Update doc-base sections.

 -- gregor herrmann <gregoa@debian.org>  Sat, 26 Feb 2011 19:31:33 +0100

fullquottel (0.1.2-1) unstable; urgency=low

  * New upstream release (closes: #417186).

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Mon, 02 Apr 2007 00:10:03 +0200

fullquottel (0.1.1-6) unstable; urgency=low

  * Update to Standards-Version: 3.7.2 (no changes required).
  * Register documentation with doc-base.
  * Add watch file.
  * Add Tony Mancill to Uploaders.
  * Improve whatis entry in manpage.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sat,  9 Sep 2006 17:43:25 +0200

fullquottel (0.1.1-5) unstable; urgency=low

  * Clarified short and long description, thanks to Anthony DeRobertis and
    Adam D. Barratt (closes: #356375).

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sun, 12 Mar 2006 17:38:28 +0100

fullquottel (0.1.1-4) unstable; urgency=low

  * Initial Debian release (closes: #347826).
  * Set debhelper compatibility level to 5.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Thu, 12 Jan 2006 22:39:57 +0100

fullquottel (0.1.1-3) unstable; urgency=low

  * Added homepage to debian/control.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sun, 25 Dec 2005 14:31:54 +0100

fullquottel (0.1.1-2) unstable; urgency=low

  * Fixed errors in Description.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Mon, 21 Nov 2005 21:59:01 +0100

fullquottel (0.1.1-1) unstable; urgency=low

  * New upstream release

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Fri,  2 Sep 2005 16:42:44 +0200

fullquottel (0.1.0-2) unstable; urgency=low

  * Clarified copyright statement in debian/copyright.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Wed, 31 Aug 2005 16:59:47 +0200

fullquottel (0.1.0-1) unstable; urgency=low

  * Initial release.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sun,  7 Aug 2005 20:54:53 +0200
